function [cn] = fourierSeries(f,N)
% computes an N-term approximate Fourier series
% representation of function handle f 
% returned as a 2xN array of the even and odd (cosine and sine)
% coefficients
%  coeffs computed using integral command
    
    % number of points of trapezoidal rule
    % (not used here)
    M = N-50;
    
    cn = zeros(2,N);
    cn(1,1) = fourierCoeff(f,M,0,'','int');
   
    for i=2:N
        cn(1,i) = fourierCoeff(f,M,i-1,'even','int');
        cn(2,i) = fourierCoeff(f,M,i-1,'odd','int');
    end

end
