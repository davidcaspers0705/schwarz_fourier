function [cnew] = fourierUpdate_quad(f,cn,N,K)
% Using the existing Fourier expansion with coefficients cn,
%  computes an additional N Fourier terms and returns all 
%  (length(cn)+N) pairs of coefficients
%  param K = number of points to be used in trapz
    M = size(cn,2);
    cnew = zeros(2,M+N);
    cnew(:,1:M) = cn;
    for i=1:N
        cnew(1,M+i) = fourierCoeff(f,K,M+i-1,'even','trapez');
        cnew(2,M+i) = fourierCoeff(f,K,M+i-1,'odd','trapez');
    end
end