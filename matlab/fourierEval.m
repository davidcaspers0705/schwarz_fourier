function [res] = fourierEval(cn,x)
% evaluates a finite Fourier series with coefficients cn at point x.
% cn must be a 2xN array, N >= 1 with cn(2,1)=0.
    
    N = size(cn,2);
    res = zeros(size(x));

    for i=1:N
        res = res + cn(1,i).*cos((i-1).*x);
        res = res + cn(2,i).*sin((i-1).*x);
    end
end