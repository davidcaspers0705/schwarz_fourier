function [res] = solutionEval2d(cn,theta,r)
% evaluates solution to the Laplace problem
% on the unit disk with coefficients cn at point x=[theta,r].
% cn must be a 2xN array, N >= 1, with cn(2,1)=0.
    
    N = size(cn,2);
    K = length(r);
    J = length(theta);
    res = zeros(K,J);
    %theta = x(1);
    %r = x(2);
    for k=1:K
        for j=1:J
            for i=1:N
                res(k,j) = res(k,j) + r(k).^(i-1).*cn(1,i).*cos((i-1).*theta(j));
                res(k,j) = res(k,j) + r(k).^(i-1).*cn(2,i).*sin((i-1).*theta(j));
            end
        end
    end
end