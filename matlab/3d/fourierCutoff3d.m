function alm_part = fourierCutoff3d(alm,M)
% expects alm as (N+1) x (2N+1) matrix
% returns alm_part the first M+1 rows with the zero columns
% on either side cut off ( (M+1) x (2M+1) matrix )
    
    N = (length(alm)-1)/2;
    
    if (~(M <= N))
        disp(['Error: Param M must be at most N, where ' ...
            'N+1 is number of rows of param alm'])
        return
    end

    alm_part = alm(1:(M+1),N+1-M:N+1+M);

end