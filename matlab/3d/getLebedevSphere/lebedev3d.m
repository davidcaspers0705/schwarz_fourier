function int = lebedev3d(degree,N,f)
 % f= function handle f(theta,phi,r)
 % degree = lebedev rule degree
 % N = number of eval points for trapezoidal quad. in r variable
 % returns int, the integral of f over the unit ball.
 %   ( combines Lebedev in (th,phi) var with Trapezoidal in (r) var.
 %    specify a different quad rule for r variable by replacing 
 %    the variables R (quadrature points) and wR (weights) below )
    leb = getLebedevSphere(degree);
    [th,phi,~] = cart2mysph(leb.x,leb.y,leb.z);
    phi(th == 0) = pi/2;
    phi(th == pi) = pi/2;
    
    % add r part of functional determinant
    f = @(th,phi,r) f(th,phi,r).*r.^2;

    % add simple trapezoidal quadrature for r:
    R = (0:1/N:1)'; %evaluation points
    
    wR = ones(length(R),1);    %weights (here: trapezoidal)
    wR(1)=1/2;
    wR(end)=1/2;
    wR = wR./N;
    
    %matrix of evaluation points
    evalMat = zeros(length(th)*length(R),3);
    evalMat(:,1) = repmat(th,length(R),1);
    evalMat(:,2) = repmat(phi,length(R),1);
    evalMat(:,3) = repelem(R,length(phi));
    
    %corresponding weights
    weights = repmat(leb.w,length(R),1).*repelem(wR,length(phi));
    
    %quadrature formula
    %tic
    v3d = f(evalMat(:,1),evalMat(:,2),evalMat(:,3));
    %toc
    int = sum(v3d.*weights);

end