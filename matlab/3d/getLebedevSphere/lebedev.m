function int = lebedev(degree,f)
 % f= function handle f(theta,phi)
 % degree = lebedev rule degree
 % returns int, the integral of f on the sphere
 %      as approximated by the corresp. Lebedev rule
    leb = getLebedevSphere(degree);
    [th,phi,~] = cart2mysph(leb.x,leb.y,leb.z);
    phi(th == 0) = pi/2;
    phi(th == pi) = pi/2;

    int = sum(f(th,phi).*leb.w);
end