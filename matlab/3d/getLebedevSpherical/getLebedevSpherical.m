function [leb] = getLebedevSpherical(order)
% returns a struct containing LEBEDEV quadrature points and 
% weights (normalized to 4pi).
% leb.th, leb.phi, leb.w
% Example of usage:
% f=@(th,phi) sin(th).^2-cos(th).^2;
% leb = getLebedevSpherical(590);
% v = f(leb.th,leb.phi);
% int = sum(v.*leb.w)
    
    % degrees:
    degrees_valid=[ 6, 14, 26, 38, 50, 74, 86, 110, 146, 170, 194, 230, 266, 302, ...
    350, 434, 590, 770, 974, 1202, 1454, 1730, 2030, 2354, 2702, 3074, ...
    3470, 3890, 4334, 4802, 5294, 5810];

    % valid orders:
    orders_valid=[3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,35,41,47,53,59,65,71,77, ...
    83,89,95,101,107,113,119,125,131];

    if (~(ismember(order,orders_valid)))
        error(sprintf('Degree %d is not a valid choice.',order))
    end
    
    leb.th = zeros(order,1);
    leb.phi = zeros(order,1);
    leb.w = zeros(order,1);

    if (order < 10)
        filename = 'lebedev_00%d.txt';
    elseif (order < 100)
        filename = 'lebedev_0%d.txt';
    else
        filename = 'lebedev_%d.txt';
    end
    
    fileID = fopen(sprintf(filename,order),'r');
    formatSpec = '%f %f %f';
    sizeA = [3 Inf];

    A = transpose(fscanf(fileID,formatSpec,sizeA));
    leb.th = deg2rad(A(:,2));
    leb.phi = deg2rad(A(:,1));
    leb.phi = leb.phi + (leb.phi < 0).*2*pi;
    leb.w = 4*pi.*A(:,3);

end