function [int] = lebedevSpherical(order,f)
% computes Lebedev integral of f of given order

    leb = getLebedevSpherical(order);
    v = f(leb.th,leb.phi);

    int = sum(v.*leb.w);
    
end