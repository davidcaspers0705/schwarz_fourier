function [res] = solutionEval3d(alm,th,phi,r)
% evaluates the solution u corresponding to a finite Fourier series with
% coefficients alm at point (th,phi,r).
% alm must be (N+1) x (2N+1) matrix
    
    N = size(alm,1)-1;
    res = zeros(size(th));

    for l=0:N
        for m=-l:l
            res = res + alm(l+1,(N+1)+m).*harmonicY(l,m,th,phi,r);
        end
    end
end