function [theta,phi,r] = cart2mysph(x,y,z)
    % converts Cartesian coordinates x,y,z
    % to spherical coordinates according to physics convention:
    % theta = inclination, in [0,pi]
    % phi = azimuth, in [0,2pi]
    % r = radius
    % (matlab uses a different convention from what I use in the 
    %    thesis.)
    [az,el,r] = cart2sph(x,y,z);

    phi = az+(az<0).*2*pi;
    theta = pi/2-el;
    
end