function [res] = fourierEval3d(alm,th,phi)
% evaluates a finite Fourier series with coefficients alm at point (th,phi).
% alm must be (N+1) x (2N+1) matrix
    
    N = size(alm,1)-1;
    res = zeros(size(th));

    for l=0:N
        for m=-l:l
            res = res + alm(l+1,(N+1)+m).*harmonicY(l,m,th,phi);
        end
    end
end