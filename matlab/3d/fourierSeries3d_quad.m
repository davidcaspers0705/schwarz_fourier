function [alm] = fourierSeries3d_quad(f,N,n)
% computes Fourier-Laplace coefficients up to order N
% returns N x (2N+1) matrix with entries a_lm where applicable
% and zero otherwise
% integrals are approximated using TRAPZ with a resolution of n
    
    alm = zeros(N+1,2*N+1);
    TH = linspace(0,pi,n);
    PHI = linspace(0,2*pi,n);
    [th,phi] = meshgrid(TH,PHI);

    for l = 0:N
        for m = -l:l
            fun = @(th,phi) f(th,phi).*conj(harmonicY(l,m,th,phi)).*sin(th);
            %alm(l+1,(N+1)+m) = integral2(fun,0,pi,0,2*pi);
            alm(l+1,(N+1)+m) = trapz(PHI,trapz(TH,fun(th,phi),2));
        end
    end
end