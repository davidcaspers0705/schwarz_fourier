function [alm] = fourierSeriesUpdate3d(f,N,n,almOld)
% computes Fourier-Laplace coefficients up to order N
% but takes previously computed coeffs alm as given
    
    Nold = size(almOld,1)-1;
    alm = zeros(N+1,2*N+1);
    alm(1:Nold+1,(N-Nold)+1:(N+Nold)+1) = almOld;

    TH = linspace(0,pi,n);
    PHI = linspace(0,2*pi,n);
    [th,phi] = meshgrid(TH,PHI);

    for l = Nold+1:N
        for m = -l:l
            fun = @(th,phi) f(th,phi).*conj(harmonicY(l,m,th,phi)).*sin(th);
            %tic
            %alm(l+1,(N+1)+m) = integral2(fun,0,pi,0,2*pi,'RelTol',1E-5);
            %toc
            alm(l+1,(N+1)+m) = trapz(PHI,trapz(TH,fun(th,phi),2));
        end
    end
end