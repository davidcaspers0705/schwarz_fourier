function [alm] = fourierSeries3d_lebedev(f,N)
% computes Fourier-Laplace coefficients up to order N
% returns N x (2N+1) matrix with entries a_lm where applicable
% and zero otherwise
% integrals are approximated using Lebedev quadrature of appropriate degree
    
    alm = zeros(N+1,2*N+1);
    %TH = linspace(0,pi,n);
    %PHI = linspace(0,2*pi,n);
    %[th,phi] = meshgrid(TH,PHI);

    %determine Lebedev quadrature degree
    % (could add an automatic decision based on some rules here;
    %   for now the orders and degrees are just listed so you can
    %   pick a degree to set below.)
    order=[3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,35,41,47,53,59,65,71,77, ...
    83,89,95,101,107,113,119,125,131];
    degree=[ 6, 14, 26, 38, 50, 74, 86, 110, 146, 170, 194, 230, 266, 302, ...
    350, 434, 590, 770, 974, 1202, 1454, 1730, 2030, 2354, 2702, 3074, ...
    3470, 3890, 4334, 4802, 5294, 5810];
    
    %for now: use order 53 rule
    D = 5810;
    LEB = getLebedevSphere(D);
    [th,phi,~] = cart2mysph(LEB.x,LEB.y,LEB.z);
    phi(th == 0) = pi/2;
    phi(th == pi) = pi/2;

    for l = 0:N
        for m = -l:l
            % note the difference: Lebedev quadrature needs only the
            % integrand itself without the functional determinant
            %fun = @(th,phi) f(th,phi).*conj(harmonicY(l,m,th,phi)).*sin(th);
            fun = @(th,phi) f(th,phi).*conj(harmonicY(l,m,th,phi));

            % Trapz, integral2 also possible; but not as accurate (trapz)
            %  or fast (integral2)
            %alm(l+1,(N+1)+m) = integral2(fun,0,pi,0,2*pi);
            %alm(l+1,(N+1)+m) = trapz(PHI,trapz(TH,fun(th,phi),2));
            alm(l+1,(N+1)+m) = sum(fun(th,phi).*LEB.w); %Lebedev quadrature
        end
    end
end