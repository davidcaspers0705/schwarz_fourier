function [alm] = fourierSeries3d(f,N)
% computes Fourier-Laplace coefficients up to order N
% returns (N+1) x (2N+1) matrix with entries a_lm where applicable
% and zero otherwise
    
    alm = zeros(N+1,2*N+1);

    for l = 0:N
        for m = -l:l
            fun = @(th,phi) f(th,phi).*conj(harmonicY(l,m,th,phi)).*sin(th);
            %tic
            alm(l+1,(N+1)+m) = integral2(fun,0,pi,0,2*pi);
            %toc
        end
    end
end