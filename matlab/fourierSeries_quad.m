function [cn] = fourierSeries_quad(f,N,M)
% computes an N-term approximate Fourier series
% representation of function handle f 
% returned as a 2xN array of the even and odd (cosine and sine)
% coefficients
%  coeffs computed using trapz command
% param M = number of points to be used in trapz
    
    %M = N-50;
    
    cn = zeros(2,N);
    cn(1,1) = fourierCoeff(f,M,0,'','trapez');
   
    for i=2:N
        cn(1,i) = fourierCoeff(f,M,i-1,'even','trapez');
        cn(2,i) = fourierCoeff(f,M,i-1,'odd','trapez');
    end

end