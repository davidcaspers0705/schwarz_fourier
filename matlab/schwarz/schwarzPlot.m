function [] = schwarzPlot(cn1,cn2,c1,r1,c2,r2,varargin)
% Produces an intermittent solution plot of
%  some result (cn1,cn2) of a Schwarz method
%  applied to two balls B_r1(c1), B_r2(c2)
% Optional arguments:
%   1. plot_overlap
%      = 1: plots solutions on ball1 and ball2 with overlap. (default)
%      = 0: does not plot ball1 solution at points contained in ball2
%           (produces some visual anomalies, but roughly correct)

    plot_overlap = 1;
    if nargin > 6
        plot_overlap = 0;
    end


    cx1 = c1(1);
    cy1 = c1(2);
    cx2 = c2(1);
    cy2 = c2(2);

    % parameters for plots
    R = 30;                       % r var discretisation
    M = 50;                       % th var discretisation
    d_theta = -pi:(2*pi)/M:pi;
    d_r = 0:1/R:1;

    


    u1 = solutionEval2d(cn1,d_theta,d_r);
    u2 = solutionEval2d(cn2,d_theta,d_r);

    [d_theta_grid,d_r_grid] = meshgrid(-pi:(2*pi)/M:pi,0:1/R:1);
    %[d_theta_grid1,d_r_grid1] = meshgrid(d_theta,d_r);
    
    if (~plot_overlap)
        % for the solution on ball1, we do not want to plot any points
        % contained in ball2 -- in the Schwarz method, after a full iteration,
        % the current approx value in ball2 is always that of the solution
        % on the second ball.
        % so, we compute an alternative d_theta, d_r excluding any ball2 pts
    
        [dx1,dx2] = pol2cart(d_theta_grid,d_r_grid);
        dx1 = (cx1+r1.*dx1);      % converting to ball1 geometry
        dx2 = (cy1+r1.*dx2);
    
        mask = sqrt((dx1-cx2).^2+(dx2-cy2).^2) >= r2;    % disregard ball2 pts
        dx1 = dx1(mask);
        dx2 = dx2(mask);
        
        dx1 = (dx1-cx1)./r1;           % converting back to origin geom
        dx2 = (dx2-cy1)./r1;
    
    
        %[d_theta_grid1,d_r_grid1] = cart2pol(dx1,dx2);   %conv to polar
        
        % now consider only the u1 points contained in this new grid
        u1 = u1(mask);
    end


    [x,y] = pol2cart(d_theta_grid,d_r_grid);
    %[x1,y1] = pol2cart(d_theta_grid1,d_r_grid1);
    if (~plot_overlap)
        u1 = griddata(dx1,dx2,u1,x,y);
    end

    figure
    % solution on ball 1
    surf(cx1 + r1.*x, cy1 + r1.*y, u1)    % solutions plot + circles
    hold on
    % solution on ball 2
    surf(cx2 + r2.*x, cy2 + r2.*y, u2)


    % PLOTTING DOMAIN OMEGA
    % compute intersection points
    [sx,sy] = circcirc(cx1,cy1,r1,cx2,cy2,r2);
    %sx, sy;
    
    %switch
    %sx = fliplr(sx);
    %sy = fliplr(sy);
    %sx, sy
    
    % theta wrt. first circle
    sx11 = sx(1)-cx1;
    sy11 = sy(1)-cy1;
    sx21 = sx(2)-cx1;
    sy21 = sy(2)-cy1;
    
    th11 = cart2pol(sx11,sy11);
    th21 = cart2pol(sx21,sy21);
    
    % it must hold (in our assumption) th21 < th11
    if (th21 > th11)
        temp = th21;
        th21 = th11;
        th11 = temp;
    end
    
    % theta wrt. second circle
    sx12 = sx(1)-cx2;
    sy12 = sy(1)-cy2;
    sx22 = sx(2)-cx2;
    sy22 = sy(2)-cy2;
    
    th12 = cart2pol(sx12,sy12);
    th22 = cart2pol(sx22,sy22);
    
    % it must hold (in our assumption) th22 < th12
    if (th22 > th12)
        temp = th22;
        th22 = th12;
        th12 = temp;
    end

    R = 50;
    M = 50;
    
    %for first circle
    if ((abs(pi-th11) + abs(-pi-th21)) > abs(th21-th11))
        d_theta11 = th11:abs(pi-th11)/M:pi;
        d_theta21 = -pi:abs(-pi-th21)/M:th21;
    else
        d_theta11 = th21:abs(th21-th11)/M:th11;
        d_theta21 = th11;
    end
    
    %for second circle
    if ((abs(pi-th12) + abs(-pi-th22)) > abs(th22-th12))
        d_theta12 = th12:abs(pi-th12)/M:pi;
        d_theta22 = -pi:abs(-pi-th22)/M:th22;
    else
        d_theta12 = th22:abs(th22-th12)/M:th12;
        d_theta22 = th12;
    end
    
    %both
    d_r = 0:1/R:1;
    
    % first circle
    plot3(cx1 + r1.*cos(d_theta11),cy1 + r1.*sin(d_theta11),zeros(length(d_theta11)), ...
        'LineWidth',1.5,'Color','r')
    hold on
    plot3(cx1 + r1.*cos(d_theta21),cy1 + r1.*sin(d_theta21),zeros(length(d_theta21)), ...
        'LineWidth',1.5,'Color','r')
    % second circle
    plot3(cx2 + r2.*cos(d_theta12),cy2 + r2.*sin(d_theta12),zeros(length(d_theta12)), ...
        'LineWidth',1.5,'Color','r')
    plot3(cx2 + r2.*cos(d_theta22),cy2 + r2.*sin(d_theta22),zeros(length(d_theta22)), ...
        'LineWidth',1.5,'Color','r')

    % END PLOTTING DOMAIN OMEGA

    % coord axes
    %xline(0,'LineWidth',1.5)
    %yline(0,'LineWidth',1.5)
    hold off
    plotConfig(gca);
    xlabel('x');
    ylabel('y');
    set(gca, 'ZMinorTick','on')
end