% Plots two circles.

cx1 = -0.5;
cy1 = 0;
cx2 = 0.5;
cy2 = 0;

% plotting our circles:
R = 50;
M = 50;
d_theta = -pi:(2*pi)/M:pi;
d_r = 0:1/R:1;

plot3(cx1 + r1.*cos(d_theta),cy1 + r1.*sin(d_theta),zeros(length(d_theta)), ...
    'LineWidth',1.5,'Color','r')
hold on
plot3(cx2 + r2.*cos(d_theta),cy2 + r2.*sin(d_theta),zeros(length(d_theta)), ...
    'LineWidth',1.5,'Color','r')
% coord axes
%xline(0,'LineWidth',1.5)
%yline(0,'LineWidth',1.5)
hold off

plotConfig(gca)
xlabel('x')
ylabel('y')
set(gca, 'ZMinorTick','on')
