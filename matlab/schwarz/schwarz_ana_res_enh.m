function [cn1,cn2] = schwarz_ana_res_enh(f,K,N,c1,r1,c2,r2,m,Nmax,varargin)
% SCHWARZ performs K iterations of the Schwarz algorithm to 
%  solve Laplace problem with boundary condition f (function handle),
%  on a domain consisting of the union of two balls with
%  centres c1 = (c1x,c1y), c2 = (c2x,c2y), radii r1, r2.
%  Each individual solve uses N-term truncated Fourier sum.
% Output cn1, cn2 are the solution coefficients describing the solutions
%  on ball 1 and 2, respectively.
% Optional arguments:
%  1. p; p=1 display plots
%        p=0 disable plots (default)
% This is the ANALYSIS (ANA) variant of the method which returns not only
% the final cn1, cn2, but all of them from previous iteration steps
%  for further computations
% cn1, cn2 are (2*K)xN matrices
%
% This is the "resolution enhancer" version of the method:
% -> two additional arguments M, Nmax.
% After K initial iterations using degree N,
%  this method performs additional M iterations using
%  a (usually considerably higher) degree Nmax.
    
    %default parameter values
    p = 0;


    if nargin > 7
        p = varargin{1};
    end

    % parameters for plots
    R = 50;
    M = 500;
    d_theta = -pi:(2*pi)/M:pi;
    d_r = 0:1/R:1;
    
    cx1 = c1(1);
    cy1 = c1(2);
    cx2 = c2(1);
    cy2 = c2(2);


    % coefficient matrices
    cn1 = zeros(2*K,Nmax);
    cn2 = zeros(2*K,Nmax);

    % Initial solution estimates for balls 1,2
    %w1 = @(x,y) 0;   %not needed: first estimate on w1 is the solution of
                      %the first local problem
    w2 = @(x,y) f(x,y);
    %w2 = @(x,y) 0;

    for i=1:K+m    %main loop

        % if in last m iterations
        % -> make more precise spherical harmonics approximation
        if (i == K+1)
            N = Nmax;
        end

        %fprintf('N=%d\n',N)
        %fprintf('m=%d\n',m)

        % SOLVE ON BALL 1
        % define boundary condition:
        f1 = @(x,y) f_ball1(f,w2,cx2,cy2,r2,x,y);
        f1 = @(x,y) arrayfun(f1,x,y);               %vectorisation
        
        % shift f1 so that c1 becomes the origin
        % and make it a function dependent on theta
        f_sh = @(x,y) f1(x+cx1,y+cy1);
        f_sh_pol = @(th) f_sh(r1.*cos(th),r1.*sin(th));

        % test plot of f_sh_pol (disabled for now)
        if (p)
            %{
            figure
            plot3(cx1+cos(d_theta),cy1+sin(d_theta),f_sh_pol(d_theta))
            hold on
            plot3(cx1 + r1.*cos(d_theta),cy1 + r1.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            plot3(cx2 + r2.*cos(d_theta),cy2 + r2.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            hold off
            %}
        end
    
        %fprintf('Iteration i=%d\n',i)
        %tic
        cn1((2*i-1):2*i,1:N) = fourierSeries(f_sh_pol,N);
        %toc
        
        err_fun = @(th) (fourierEval(cn1(2*i-1:2*i,:),th)-f_sh_pol(th)).^2;
        %err = integral(err_fun,-pi,pi)
        %err_trapz = trapz(d_theta,err_fun(d_theta))

        
        
        if (p)
            % plot f compared to Fourier series in 1D
            % theta values of the circle intersection points 
            %  (for the standard example from the thesis)
            
            th12 = 1.047197551196598;
            th22 = -th12;
            
            % comment out the following lines if you want a full plot
            %  of f; here I do only a plot very close to the first
            %  intersection point to see the non-smoothness better.
            %eps = 2E-1;
            %d_theta = th22-eps:(2*eps)/M:th22+eps;
            %
            
            figure
            plot(d_theta,f_sh_pol(d_theta),'LineWidth',1.5);
            hold on
            plot(d_theta,fourierEval(cn1(2*i-1:2*i,:),d_theta), ...
                'LineStyle','-',...
                'LineWidth',1)
            %xline(th12,'Color','red','LineStyle','--','LineWidth',1)
            xline(th22,'Color','red','LineStyle','--','LineWidth',1)
            hold off
            plotConfig(gca)
            xlabel('\theta')
            ylabel('f(\theta)')
            %xticks(-pi:pi/2:pi)
            %xticklabels({'-\pi','-\pi/2','0','\pi/2','\pi'})
            legend('Exact $f$',...
                sprintf('$N=%d$ Fourier sum',N),...
                'interpreter','latex', ...
                'location','best')
            title('Exact boundary condition compared to Fourier approximation on $\Omega_1$', ...
                'interpreter','latex')
        end

        if (p)
            %plot the solution on ball 1
            %{
            u = solutionEval2d(cn1((2*i-1):2*i,:),d_theta,d_r);
            [d_theta_grid,d_r_grid] = meshgrid(-pi:(2*pi)/M:pi,0:1/R:1);
            [x,y] = pol2cart(d_theta_grid,d_r_grid);
            figure
            surf(cx1 + r1.*x, cy1 + r1.*y, u)    % solution plot + circles
            hold on
            plot3(cx1 + r1.*cos(d_theta),cy1 + r1.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            plot3(cx2 + r2.*cos(d_theta),cy2 + r2.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            hold off
            plotConfig(gca);
            xlabel('x');
            ylabel('y');
            title(sprintf('Iteration %d, Ball 1 solved',i))
            %}
        end

        % UPDATE w1 BY SOLUTION ON BALL1 
        % (shifted according to center1!)
        w1 = @(x,y) solEvalCart(cn1((2*i-1):2*i,:),(x-cx1)./r1,(y-cy1)./r1);
        

        % SOLVE ON BALL 2
        % define boundary condition:
        f2 = @(x,y) f_ball2(f,w1,cx1,cy1,r1,x,y);
        f2 = @(x,y) arrayfun(f2,x,y);              %vectorisation
        
        % shift f so that c2 becomes the origin
        % and make it a function dependent on theta
        f_sh = @(x,y) f2(x+cx2,y+cy2);
        f_sh_pol = @(th) f_sh(r2.*cos(th),r2.*sin(th));
        
        %input = [-pi; pi];
        %cos(input),sin(input)
        %f_sh(cos(input),sin(input))


        % test plot of f_sh_pol (disabled for now)
        if (p)
            %{
            figure
            plot3(cx2+cos(d_theta),cy2+sin(d_theta),f_sh_pol(d_theta))
            hold on
            plot3(cx1 + r1.*cos(d_theta),cy1 + r1.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            plot3(cx2 + r2.*cos(d_theta),cy2 + r2.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            hold off
            %}
        end
        
        %tic
        cn2((2*i-1):2*i,1:N) = fourierSeries(f_sh_pol,N);
        %toc
        %sprintf('--------------')

        if (p)
            % plot f compared to Fourier series in 1D
            % theta values of the circle intersection points 
            %  (for the standard example from the thesis)
            th11 = 2.094395102393196;
            th21 = -th11;
            % comment out the following lines if you want a full plot
            %  of f; here I do only a plot very close to the first
            %  intersection point to see the non-smoothness better.
            %eps = 2E-1;
            %d_theta = th11-eps:(2*eps)/M:th11+eps;
            %

            figure
            plot(d_theta,f_sh_pol(d_theta),'LineWidth',1.5);
            hold on
            plot(d_theta,fourierEval(cn2(2*i-1:2*i,:),d_theta), ...
                'LineStyle','-',...
                'LineWidth',1)
            xline(th11,'Color','red','LineStyle','--','LineWidth',1)
            %xline(th21,'Color','red','LineStyle','--','LineWidth',1)
            hold off
            plotConfig(gca)
            xlabel('\theta')
            ylabel('f(\theta)')
            %xticks(-pi:pi/2:pi)
            %xticklabels({'-\pi','-\pi/2','0','\pi/2','\pi'})
            legend('Exact $f$',...
                sprintf('$N=%d$ Fourier sum',N),...
                'interpreter','latex', ...
                'location','best')
            title('Exact boundary condition compared to Fourier approximation on $\Omega_2$', ...
                'interpreter','latex')
        end

        if (p)
            %plot the solution on ball 2
            %{
            u = solutionEval2d(cn2((2*i-1):2*i,:),d_theta,d_r);
            
            figure
            surf(cx2 + r2.*x, cy2 + r2.*y, u)    % solution plot + circles
            hold on
            plot3(cx1 + r1.*cos(d_theta),cy1 + r1.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            plot3(cx2 + r2.*cos(d_theta),cy2 + r2.*sin(d_theta),zeros(length(d_theta)), ...
                'LineWidth',1.5,'Color','r')
            hold off
            plotConfig(gca);
            xlabel('x');
            ylabel('y');
            title(sprintf('Iteration %d, Ball 2 solved',i))
            %}
        end

        % UPDATE w2 BY SOLUTION ON BALL2 
        % (shifted according to center2!)
        w2 = @(x,y) solEvalCart(cn2((2*i-1):2*i,:),(x-cx2)./r2,(y-cy2)./r2);

    end
    
end


function res = f_ball1(f,w,cx2,cy2,r2,x,y)
% defines custom boundary condition for ball1:
% if x,y contained in second ball: return w(x,y) (previous estimate)
% otherwise return global boundary condition f.

    res = (sqrt((cx2-x).^2+(cy2-y).^2) <= r2) .* w(x,y)...
        + (sqrt((cx2-x).^2+(cy2-y).^2) > r2) .* f(x,y);

end

function res = f_ball2(f,w,cx1,cy1,r1,x,y)
% defines custom boundary condition for ball2:
% if x,y contained in first ball: return w(x,y) (previous estimate)
% otherwise return global boundary condition f.

    res = (sqrt((cx1-x).^2+(cy1-y).^2) <= r1) .* w(x,y)...
        + (sqrt((cx1-x).^2+(cy1-y).^2) > r1) .* f(x,y);

end

function res = solEvalCart(cn1,x,y)
% simply converts x,y to polar coords and passes
% to solutionEval2d function
    [th,r] = cart2pol(x,y);
    res = solutionEval2d(cn1,th,r);
end