function [cn] = fourierCoeff(f,N,n,oe,mode)
% Computes approximate Fourier coefficient for function f
%  (f given as function handle)
% based on numerical quadrature approximation
% if oe='even', computes cosine coeff a_n
% if oe='odd', computes sine coeff b_n
% if mode='trapez', uses trapezoidal rule with N+1 points
% if mode='int', uses matlab adaptive quadrature (and ignores param N)
    
    if (strcmp(mode,'trapez'))
        %synthesise data from function handle f
        domain = [-pi:(2*pi)/N:pi];
        val = f(domain);
        if (n == 0)
            cn = trapz(domain,val) / (2*pi);
        elseif (strcmp(oe,'even'))
            val = val .* cos(n.*domain);
            cn = trapz(domain,val) / pi;
        elseif (strcmp(oe,'odd'))
            val = val .* sin(n.*domain);
            cn = trapz(domain,val) / pi;
        else
            disp('input argument error in param oe')
        end

    elseif(strcmp(mode,'int'))
        if (n == 0)
            cn = integral(f,-pi,pi) / (2*pi);
        elseif (strcmp(oe,'even'))
            g = @(x) f(x).*cos(n.*x);
            cn = integral(g,-pi,pi) / pi;
        elseif (strcmp(oe,'odd'))
            g = @(x) f(x).*sin(n.*x);
            cn = integral(g,-pi,pi) / pi;
        else
            disp('input argument error in param oe')
        end
    
    else
        disp('input argument error in param mode')

    end
end