function [cn_part] = fourierCutoff(cn,M)
% expects 2xN array cn,
% returns first M+1 columns of cn
% (cuts off after Fourier coefficient of order M)

    N = length(cn);

    if (~(M+1 <= N))
        disp(['Error: param M must be at most N-1,' ...
            'where N is length of param cn'])
        return
    end

    cn_part = cn(:,1:M+1);
end