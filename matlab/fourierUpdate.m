function [cnew] = fourierUpdate(f,cn,N)
% Using the existing Fourier expansion with coefficients cn,
%  computes an additional N Fourier terms and returns all 
%  (length(cn)+N) pairs of coefficients
    M = size(cn,2);
    cnew = zeros(2,M+N);
    cnew(:,1:M) = cn;
    for i=1:N
        cnew(1,M+i) = fourierCoeff(f,0,M+i-1,'even','int');
        cnew(2,M+i) = fourierCoeff(f,0,M+i-1,'odd','int');
    end
end